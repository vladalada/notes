import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';

class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        let notes = [{"title":"Foo","text":"Text","color":1},{"title":"Title","text":"Text","color":3},{"title":"Title","text":"Text","color":4},{"title":"Title","text":"Text","color":1},{"title":"Title","text":"Text","color":4}];
        if (window.localStorage && window.localStorage.getItem("notes")) {
            notes = JSON.parse(window.localStorage.getItem("notes"));
        }
        this.state = {notes: notes};
    }



    addNoteHandler() {
        const newNotes = this.state.notes.slice();
        newNotes.push({title: "Title", text: "Text", color: this.getRandomNoteColor(5)});
        this.setState({notes: newNotes});
        if (window.localStorage) {
            window.localStorage.setItem("notes", JSON.stringify(newNotes));
        }
    }

    getRandomNoteColor(num) {
        return Math.round(Math.random() * 100) % num;
    }

    showDialogHandler(index) {
        const currentNote = this.state.notes[index];
        this.setState({
            currentText: currentNote.text,
            currentTitle: currentNote.title,
            showDialog: true,
            currentIndex: index
        });
    }

    deleteNoteHandler(index) {
        const notesCopy = this.state.notes.slice();
        notesCopy.splice(index, 1);
        this.setState({notes: notesCopy});
        if (window.localStorage) {
            window.localStorage.setItem("notes", JSON.stringify(notesCopy));
        }

    }

    hideDialogHandler() {
        this.setState({showDialog: false});
    }

    changeTitleHandler(newTitle) {
        this.setState({currentTitle: newTitle});
    }

    changeTextHandler(newText) {
        this.setState({currentText: newText});
    }

    saveNoteHandler() {
        const notesCopy = this.state.notes.slice();
        const currentNote = notesCopy[this.state.currentIndex];
        currentNote.title = this.state.currentTitle;
        currentNote.text = this.state.currentText;
        this.setState({notes: notesCopy});
        this.hideDialogHandler();
        if (window.localStorage) {
            window.localStorage.setItem("notes", JSON.stringify(notesCopy));
        }
    }

    clearNoteHandler() {
        const notesCopy = this.state.notes.slice();
        const currentNote = notesCopy[this.state.currentIndex];
        currentNote.title = "";
        currentNote.text = "";
        this.setState({notes: notesCopy});
        this.hideDialogHandler();
        if (window.localStorage) {
            window.localStorage.setItem("notes", JSON.stringify(notesCopy));
        }
    }

    render() {
        let notes = [];
        for (let i = 0; i < this.state.notes.length; i++) {
            const note = this.state.notes[i];
            notes.push(<Note title={note.title}
                             text={note.text}
                             color={note.color}
                             key={i}
                             onNoteClickHandler={() => this.showDialogHandler(i)}
                             onDeleteNoteHander={() => this.deleteNoteHandler(i)}
            />);
        }
        return <div className="Dashboard">
            {notes}
            <Plus onPlusClickHandler={() => this.addNoteHandler()}/>
            <Dialog show={this.state.showDialog}
                    title={this.state.currentTitle}
                    text={this.state.currentText}
                    onClickBackgroundHandler={() => this.hideDialogHandler()}
                    onTitleChangeHandler={(value) => this.changeTitleHandler(value)}
                    onTextChangeHandler={(value) => this.changeTextHandler(value)}
                    saveHandler={() => this.saveNoteHandler()}
                    clearHandler={() => this.clearNoteHandler()}/>
        </div>;
    }

}

class Note extends React.Component {

    render() {
        return <div id="Note"
                    className={"Note" + this.props.color.toString()}
                    onClick={(e) => this.props.onNoteClickHandler()}
                    >
            <div className="DeleteNote"
                 onClick={(e) => {
                     e.stopPropagation();
                     this.props.onDeleteNoteHander()
                 }}>x
            </div>
            <h2>{this.props.title}</h2>
            <p>{this.props.text}</p>
        </div>;
    }
}

class Plus extends React.Component {
    render() {
        return <div className="Plus"
                    onClick={(e) => this.props.onPlusClickHandler()}>
            <h1>+</h1>
        </div>;
    }
}

class Dialog extends React.Component {


    render() {
        let display = "none";
        if (this.props.show) {
            display = "flex";
        }

        return <div className="Background"
                    style={{display: display}}
                    onClick={(e) => {
                        this.props.onClickBackgroundHandler();
                    }}>
            <div className="Dialog"
                 onClick={(e) => e.stopPropagation()}>
                <input className="TitleInput"
                       value={this.props.title}
                       onChange={(e) => this.props.onTitleChangeHandler(e.target.value)}/>
                <textarea
                    className="TextInput"
                    value={this.props.text}
                    onChange={(e) => this.props.onTextChangeHandler(e.target.value)}/>
                <div className="Buttons">
                    <button className="SaveButton"
                            onClick={(e) => this.props.saveHandler()}>Save
                    </button>
                    <button className="ClearButton"
                            onClick={(e) => this.props.clearHandler()}>Clear
                    </button>
                </div>
            </div>

        </div>;
    }
}

ReactDOM.render(<Dashboard/>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
